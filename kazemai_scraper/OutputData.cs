﻿using System.Collections.Generic;

namespace kazemai_scraper
{

	class Output
	{
		public float scaleFactor = 1000f;

		public Dictionary<int, OutputClass> classes;
		public Dictionary<int, string> attributeNames;

		public Dictionary<int, Dictionary<int, int>> affinities;
		public Dictionary<int, Dictionary<int, int>> attributes;

		public List<OutputServant> servants;
	}

	class OutputClass
	{
		public string name;
		public int attackMod;
	}

	class OutputNPData
	{
		public Dictionary<string, int> cardGen;
		public int npGen;
		public List<int> mods;
	}

	struct OutputServantPassives
	{
		public Dictionary<string, int> cardMods; // ME, TC, Riding etc

		public int flatDamage; // Divinity etc

		public int critDamageMod; // Independent Action etc
	}

	class OutputServant
	{
		public int id; // in-game id

		public int classId;
		public int attributeId;

		public int defaultLevelCap; // no grails

		public List<int> atkPerLevel; // 1-100 (0-99)

		public Dictionary<string, List<int>> cardHitPercentages;

		public string npCardType;

		public bool hasDamagingNP;

		public OutputNPData np;

		public OutputNPData npStrengthen;

		public List<int> npHitPercentages = new List<int>();

		public OutputServantPassives passives;

        public int starRate;

		// Kiara >_> (class id -> modifier)
		public Dictionary<int, int> affinityOverrides = new Dictionary<int, int>();
	}
}
