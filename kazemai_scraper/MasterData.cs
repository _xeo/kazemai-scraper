﻿using System.Collections.Generic;

namespace kazemai_scraper
{
	class DataMaster
	{
		public List<DataClassRelation> mstClassRelation;

		public List<DataAttributeRelation> mstAttriRelation;

		public List<DataServant> mstSvt;

		public List<DataServantExp> mstSvtExp;

		public List<DataServantValues> mstSvtLimit;

		public List<DataServantCard> mstSvtCard;

		public List<DataServantNP> mstSvtTreasureDevice;

		public List<DataNPLevel> mstTreasureDeviceLv;

		public List<DataSkill> mstSkill;

		public List<DataSkillLevel> mstSkillLv;

		public List<DataClass> classes;

		public List<DataNP> nps;
	}

	struct DataClassRelation
	{
		public int atkClass;
		public int defClass;
		public int attackRate;
	}

	struct DataAttributeRelation
	{
		public int atkAttri;
		public int defAttri;
		public int attackRate;
	}

	struct DataNP
	{
		public int id;

		public List<int> damage;
	}

	struct DataNPLevel
	{
		public int treasureDeviceId { get { return treaureDeviceId; } }

		public int treaureDeviceId;

		public int tdPoint;
		public int tdPointA;
		public int tdPointQ;
		public int tdPointB;
		public int tdPointEx;
	}

	struct DataClass
	{
		public int id;
		public int attri; // ???

		public string name;

		public int attackRate; // classMod, divide by 1000
	}

	struct DataServant
	{
		public long id; // data id

		public int collectionNo; // in-game id
		public int classId;

		public int expType; // exp curve type
		public int rewardLv; // max level

		public int attri;

		public List<long> classPassive;

		public int type; // sort out unused, NPC, enemy, CE, etc.

		public int starRate; // crit star gen, divide by 10

		public string name;

		// maybe other ids
	}

	struct DataServantValues
	{
		public int svtId; // data id of related servant
		public int limitCount; // current ascension

		public int atkBase; // base attack value at lv1
		public int atkMax; // max attack value at rewardLv
	}

	struct DataServantCard
	{
		public int svtId; // data id of related servant

		public int cardId; // card type

		public List<int> normalDamage; // hit percentages
	}

	struct DataServantNP
	{
		public int svtId; // data id of related servant
		public int treasureDeviceId; // data id of this np

		public int num; // filter out unused (= 99) NPs?

		public int priority; // usage priority

		public int cardId; // card type

		public List<int> damage; // hit percentages

		public int strengthStatus; // 0 = unavailable, 1 = available, 2 = cleared
	}

	struct DataServantExp
	{
		public int type;
		public int lv;
		//public int exp; // unused
		public int curve;
	}

	struct DataSkill
	{
		public int id;

		public int type; // 1 = active, 2 = passive?
	}

	struct DataSkillLevel
	{
		public int skillId;

		public int lv;

		public List<int> funcId; // some script to help evaluate values, assumed 1 per effect type
		public List<string> svals; // script vars, fun. :help:
	}
}
