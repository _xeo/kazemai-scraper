﻿using Newtonsoft.Json;
using OpenQA.Selenium.PhantomJS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace kazemai_scraper
{
	struct RawData
	{
		public string master;
		public string classes;
		public string tdDetails;
	}

	class Program
	{
		private static readonly string[] Attributes = new []
		{
			"man",
			"sky",
			"earth",
			"star",
			"beast"
		};

		private static readonly string[] CardTypeNames = new[]
		{
			"arts",
			"buster",
			"quick",
			"extra"
		};

		static void Main(string[] args)
		{
#if DEBUG
			RawData data;
			if (File.Exists("rawData.json"))
			{
				data = JsonConvert.DeserializeObject<RawData>(File.ReadAllText("rawData.json"));
			}
			else
			{
				data = ParseWeb();
			}
#else
			var data = ParseWeb();
#endif

			var master = JsonConvert.DeserializeObject<DataMaster>(data.master);
			master.classes = JsonConvert.DeserializeObject<List<DataClass>>(data.classes);

			var tdDetails = JsonConvert.DeserializeObject<List<List<string>>>(data.tdDetails);
			master.nps = tdDetails.Select(l => new DataNP
			{
				id = int.Parse(l[0]),
				damage = GetNpDamage(l)
			}).ToList();

			HashSet<int> usedClasses;
			var output = new Output()
			{
				servants = GatherServants(master, out usedClasses),
				classes = GatherClasses(usedClasses, master.classes),
				attributeNames = Enumerable.Range(1, 5).ToDictionary(id => id, id => Attributes[id - 1]),

				affinities = BuildAffinityTable(usedClasses, master),
				attributes = BuildAttributeTable(master)
			};
			File.WriteAllText("data.json", JsonConvert.SerializeObject(output, Formatting.Indented));

			//output.servants.ForEach(s => Console.Out.WriteLine(s.id));
		}

		private static Dictionary<int, Dictionary<int, int>> BuildAffinityTable(HashSet<int> usedClasses, DataMaster master)
		{
			return usedClasses
				.ToDictionary(id => id, id => master.mstClassRelation
					.Where(r => r.atkClass == id && usedClasses.Contains(r.defClass))
					.ToDictionary(r => r.defClass, r => r.attackRate));
		}

		private static Dictionary<int, Dictionary<int, int>> BuildAttributeTable(DataMaster master)
		{
			return Enumerable.Range(1, 5)
				.ToDictionary(id => id, id => master.mstAttriRelation
					.Where(r => r.atkAttri == id)
					.ToDictionary(r => r.defAttri, r => r.attackRate));
		}

		private static List<OutputServant> GatherServants(DataMaster master, out HashSet<int> usedClasses)
		{
			usedClasses = new HashSet<int>{};

			var servants = new List<OutputServant>(250);
			foreach (var svtData in master.mstSvt)
			{
				// 1 == normal servant, 2 == Mash
				bool unsummonable = svtData.type != 1 && svtData.type != 2;
				if (unsummonable || svtData.collectionNo == 0)
					continue;

				var servant = new OutputServant
				{
					id = svtData.collectionNo,
					classId = svtData.classId,
					attributeId = svtData.attri,
					defaultLevelCap = svtData.rewardLv,
					starRate = svtData.starRate * 100
				};

				// record class
				usedClasses.Add(servant.classId);

				FillNpData(servant, master, svtData);

				servant.atkPerLevel = CalculateAttackValues(master, svtData);
				servant.cardHitPercentages = GatherCardHits(master.mstSvtCard, svtData.id);

				SkillEvaluator.Run(servant, master, svtData);

				servants.Add(servant);
			}

			return servants;
		}

		private static Dictionary<string, List<int>> GatherCardHits(List<DataServantCard> cards, long id)
		{
			return cards.Where(c => c.svtId == id)
				.ToDictionary(c => CardTypeNames[c.cardId - 1], c => c.normalDamage);
		}

		private static Dictionary<int, OutputClass> GatherClasses(HashSet<int> usedClasses, List<DataClass> classes)
		{
			return classes.Where(c => usedClasses.Contains(c.id))
				.ToDictionary(e => e.id, e => new OutputClass
				{
					name = e.name,
					attackMod = e.attackRate
				});
		}

		private static List<int> CalculateAttackValues(DataMaster master, DataServant svtData)
		{
			var values = master.mstSvtLimit.First(l => l.svtId == svtData.id);

			return Enumerable.Range(1, 100)
				.Select(level => CalculateAttackValues(master, svtData, values, level))
				.ToList();
		}

		private static int CalculateAttackValues(DataMaster master, DataServant svtData, DataServantValues values, int level)
		{
			var expData = master.mstSvtExp.First(exp => exp.type == svtData.expType && exp.lv == level);
			return values.atkBase + (values.atkMax - values.atkBase) * expData.curve / 1000;
		}

		private static void FillNpData(OutputServant servant, DataMaster master, DataServant svtData)
		{
			var npData = master.mstSvtTreasureDevice.FindAll(np => np.svtId == svtData.id && (np.num == 1 || np.num == 2));
			var npMods = master.nps.FindAll(np => npData.Any(d => d.treasureDeviceId == np.id));

			if (npMods.Count == 0)
				return; // faulty data

			servant.npCardType = CardTypeNames[npData[0].cardId - 1];

			servant.np = GetNPData(npData, npMods, master, strengthen: false);
			servant.npStrengthen = GetNPData(npData, npMods, master, strengthen: true);

			servant.hasDamagingNP = servant.np.mods != null;

			if (servant.hasDamagingNP)
				servant.npHitPercentages = npData[0].damage?.ToList() ?? new List<int>();
		}

		private static OutputNPData GetNPData(List<DataServantNP> npData, List<DataNP> npMods, DataMaster master, bool strengthen)
		{
			var unstrengthened = npData.All(data => data.strengthStatus == 0);

			if (strengthen && unstrengthened)
				return null; // mismatch

			// find the highest priority NP data that fits the requested strengthen state
			var desiredStatus = unstrengthened ? 0 : (strengthen ? 2 : 1);
			var specificData = npData.OrderByDescending(data => data.priority).First(data => data.strengthStatus == desiredStatus);
			var np = npMods.First(data => data.id == specificData.treasureDeviceId);

			var lvData = master.mstTreasureDeviceLv.Find(lv => lv.treasureDeviceId == np.id);
			return new OutputNPData
			{
				mods = np.damage,
				npGen = lvData.tdPoint,
				cardGen = new Dictionary<string, int>
				{
					{ "buster", lvData.tdPointB },
					{ "arts", lvData.tdPointA },
					{ "quick", lvData.tdPointQ },
					{ "extra", lvData.tdPointEx }
				}
			};
		}

		private static List<int> GetNpDamage(List<string> l)
		{
			foreach (var line in l)
			{
				var split = line.Split('/');

				if (split.Length == 1 && split[0].EndsWith("%"))
				{
					// special-case Eury-like NPs with only one damage number, extend to 5
					// this will allow some unused NP data to slip through, but that's fine
					split = new[] { split[0], split[0], split[0], split[0], split[0] };
				}

				if (split.Length != 5)
					continue;

				if (!split.All(s => s.EndsWith("%")))
					continue;

				var values = split
					.Select(s => float.Parse(s.TrimEnd('%')))
					.Select(f => (int)(f * 10))
					.ToList();

				if (values[0] < 3000)
					continue;

				return values;
			}
			return null;
		}

		static RawData ParseWeb()
		{
			var driver = new PhantomJSDriver
			{
				Url = "http://kazemai.github.io/fgo-vz/svtData.html"
			};

			try
			{
				driver.Navigate();

				var data = new RawData
				{
					master = driver.ExecuteScript("return JSON.stringify(master);") as string,
					classes = driver.ExecuteScript("return JSON.stringify(mstClass);") as string,
					tdDetails = driver.ExecuteScript("return JSON.stringify(tdDetail);") as string
				};

#if DEBUG
				File.WriteAllText("rawData.json", JsonConvert.SerializeObject(data));
#endif

				return data;
			}
			finally
			{
				driver.Quit();
			}
		}
	}
}
