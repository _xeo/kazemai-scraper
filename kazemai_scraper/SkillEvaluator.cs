﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace kazemai_scraper
{
	using FuncDict = Dictionary<int, Action<SkillEvaluator.Mods, OutputServant, int>>;
	class SkillEvaluator
	{
		private static FuncDict Funcs = new FuncDict();

		/*
		 * 0: 349250
		 * 1: 161150
		 * 2: 350350
		 * 3: 351550
		 */

		public class Mods
		{
			public Dictionary<string, int> cards = new Dictionary<string, int>
			{
				{ "buster", 0 }, { "arts", 0 }, { "quick", 0 }
			};
			public int crit = 0;
			public int flat = 0;

			public Dictionary<int, int> affinity = new Dictionary<int, int>();
		}

		static SkillEvaluator()
		{
			Funcs[100] = CardMod("quick");
			Funcs[109] = CardMod("arts");
			Funcs[118] = CardMod("buster");
			Funcs[199] = CritMod;
			Funcs[265] = FlatDamage; // divinity
			Funcs[1481] = NegaSaver; // Kiara
		}

		public static void Run(OutputServant servant, DataMaster master, DataServant svtData)
		{
			var mods = new Mods();
			foreach (var skillId in svtData.classPassive)
			{
				var skill = master.mstSkillLv.Find(e => e.skillId == skillId);

				foreach (var i in Enumerable.Range(0, skill.funcId.Count))
				{
					var funcId = skill.funcId[i];

					Action<Mods, OutputServant, int> func;
					if (Funcs.TryGetValue(funcId, out func))
					{
						var rawValues = skill.svals[i].Trim('[', ']').Split(',');
						func(mods, servant, int.Parse(rawValues[3]));
					}
				}
			}

			servant.passives.cardMods = mods.cards;
			servant.passives.flatDamage = mods.flat;
			servant.passives.critDamageMod = mods.crit;
			servant.affinityOverrides = mods.affinity;
		}

		#region skills
		// note: add values, as there may be multiple skills affecting the same mod

		private static Action<Mods, OutputServant, int> CardMod(string cardType)
		{
			return (mods, servant, value) => { mods.cards[cardType] += value; };
		}

		private static void CritMod(Mods mods, OutputServant servant, int value)
		{
			mods.crit += value;
		}

		private static void FlatDamage(Mods mods, OutputServant servant, int value)
		{
			mods.flat += value;
		}

		private static void NegaSaver(Mods mods, OutputServant servant, int value)
		{
			mods.affinity[9] = value; // vs ruler
		}

		#endregion
	}
}
